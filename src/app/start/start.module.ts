import { SharedModule } from "./../shared/shared.module";
import { ROUTES } from "./start.routes";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StartContainerComponent } from "./container/start-container/start-container.component";
import { LocalizeRouterModule } from "localize-router";

@NgModule({
  declarations: [StartContainerComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
    LocalizeRouterModule.forChild(ROUTES)
  ]
})
export class StartModule {}
