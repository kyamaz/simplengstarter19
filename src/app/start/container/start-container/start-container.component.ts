import { CustomFormsModule } from "@forms/customForms.module";
import { FormsService } from "@forms/forms.services";
import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
@Component({
  selector: "app-start-container",
  templateUrl: "./start-container.component.html",
  styleUrls: ["./start-container.component.scss"],
  providers: [CustomFormsModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StartContainerComponent implements OnInit {
  constructor(public formsService: FormsService<{ name: string }>) {}

  ngOnInit() {
    console.log("couou");
    console.log(this.formsService.build_form({ name: "test" }));
  }
}
